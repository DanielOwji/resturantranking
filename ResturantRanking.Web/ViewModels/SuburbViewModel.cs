﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ResturantRanking.Web.ViewModels
{
    public class SuburbViewModel
    {
        public int SuburbID { get; set; }
        public string SuburbName { get; set; }
        public string PostCode { get; set; }

        public List<ResturantViewModel> Resturants { get; set; }
    }
}
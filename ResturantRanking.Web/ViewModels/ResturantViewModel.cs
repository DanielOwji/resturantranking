﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ResturantRanking.Model.Models;

namespace ResturantRanking.Web.ViewModels
{
    public class ResturantViewModel 
    {
        public int ResturantID { get; set; }
        public string ResturantName { get; set; }
        public string Image { get; set; }
        public int StarRating { get; set; }
        public string Latidtude { get; set; }
        public string Longitude { get; set; }
        public int Rating { get; set; }
        public decimal AveragePrice { get; set; }
        public virtual List<OrderDetailViewModel> OrdersDetails { get; set; }
        public double Ranking { get; set; }
        public int SuburbID { get; set; }
        public decimal distance { get; set; }        
    }
}
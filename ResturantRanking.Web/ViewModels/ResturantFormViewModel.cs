﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ResturantRanking.Web.ViewModels
{
    public class ResturantFormViewModel
    {
        public HttpPostedFileBase File { get; set; }
        public string ResturantName { get; set; }
        public decimal AveragePrice { get; set; }
        public int ResturantSuburb { get; set; }
        public int StarRating { get; set; }
    }

}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ResturantRanking.Web.ViewModels
{
    public class OrderDetailViewModel
    {
        public int OrderDetailID { get; set; }
        public decimal Price { get; set; }
        public DateTime OrderTime { get; set; }
        public int ResturantID { get; set; }
        
    }
}
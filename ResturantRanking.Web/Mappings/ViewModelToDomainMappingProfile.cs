﻿using AutoMapper;
using ResturantRanking.Model;
using ResturantRanking.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ResturantRanking.Model.Models;

namespace ResturantRanking.Web.Mappings
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public override string ProfileName => "ViewModelToDomainMappings";
        public ViewModelToDomainMappingProfile()
        {
            CreateMap<ResturantFormViewModel, Resturant>()
                .ForMember(g => g.ResturantName, map => map.MapFrom(vm => vm.ResturantName))
                .ForMember(g => g.AveragePrice, map => map.MapFrom(vm => vm.AveragePrice))
                .ForMember(g => g.Image, map => map.MapFrom(vm => vm.File.FileName))
                .ForMember(g => g.Suburb, map => map.MapFrom(vm => vm.ResturantSuburb))
                .ForAllOtherMembers(x=>x.Ignore());


        }        
    }
}
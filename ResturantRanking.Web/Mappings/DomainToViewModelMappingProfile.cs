﻿using AutoMapper;
using ResturantRanking.Model;
using ResturantRanking.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ResturantRanking.Model.Models;

namespace ResturantRanking.Web.Mappings
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public DomainToViewModelMappingProfile()
        {
            CreateMap<Resturant, ResturantViewModel>();
            CreateMap<Suburb, SuburbViewModel>();
            CreateMap<OrderDetail, OrderDetailViewModel>();
        }
        public override string ProfileName => "DomainToViewModelMappings";
    }
}
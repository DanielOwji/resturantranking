﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ResturantRanking.Web.ViewModels;
using WebGrease.Css.Extensions;

namespace ResturantRanking.Web.Helpers
{
    public class HomeHelper
    {
        public static IEnumerable<SuburbViewModel> Rank(IEnumerable<SuburbViewModel> suburbs, double latitude, double longitude)
        {
            var suburbViewModels = suburbs as IList<SuburbViewModel> ?? suburbs.ToList();
            suburbViewModels.ForEach(x =>
            {
                x.Resturants?.ForEach(f =>
                {
                    f.distance = calcDistance(Convert.ToDouble(f.Latidtude), Convert.ToDouble(f.Longitude), latitude, longitude);
                    f.Ranking = (double) ((f.AveragePrice * (decimal) (-0.5)) + f.StarRating +
                                          GetOrderRanking(f.OrdersDetails)) / 3 ;

                });
            });

            return suburbViewModels;
        }

        private static decimal GetOrderRanking(IEnumerable<OrderDetailViewModel> resturantOrdersDetails)
        {
            var sortedOrders = resturantOrdersDetails.OrderBy(x => x.OrderTime);
            var t = 0;
            var counter = sortedOrders.Count() * -1;
            sortedOrders.ForEach(order =>
            {
                t += (int)(((DateTime.Now - order.OrderTime).Days / 30) + 1) * counter;
                counter++;
            });
            if (counter != 0)
                return t / sortedOrders.Count();
            return 0;
        }

        private static decimal calcDistance(double latA, double longA, double latB, double longB)
        {

            double theDistance = (Math.Sin(DegreesToRadians(latA)) *
                                  Math.Sin(DegreesToRadians(latB)) +
                                  Math.Cos(DegreesToRadians(latA)) *
                                  Math.Cos(DegreesToRadians(latB)) *
                                  Math.Cos(DegreesToRadians(longA - longB)));

            return Convert.ToDecimal((RadiansToDegrees(Math.Acos(theDistance)))) * 69.09M * 1.6093M;
        }
        private static double DegreesToRadians(double deg)
        {
            return (deg * Math.PI / 180.0);
        }        
        private static double RadiansToDegrees(double rad)
        {
            return (rad / Math.PI * 180.0);
        }
    }

}
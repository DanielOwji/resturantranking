﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using ResturantRanking.Model.Models;
using ResturantRanking.Service.Interfaces;
using ResturantRanking.Service.Services;
using ResturantRanking.Web.ViewModels;
using WebGrease.Css.Extensions;

namespace ResturantRanking.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ISuburb _suburbService;
        private readonly IResturant _resturantService;

        public HomeController(ISuburb suburbService, IResturant resturantService)
        {
            this._suburbService = suburbService;
            this._resturantService = resturantService;
        }

        public ActionResult Index(string postcode = null)
        {
            IEnumerable<Suburb> suburbs = _suburbService.GetSuburbs(postcode).ToList();
            var viewModelSuburb = Mapper.Map<IEnumerable<Suburb>, IEnumerable<SuburbViewModel>>(suburbs);
            var suburbViewModels = viewModelSuburb as IList<SuburbViewModel> ?? viewModelSuburb.ToList();

            //Hardcoded Location"-33.874163, 151.209323"
            //Ranking Returned list of Resturants
            Helpers.HomeHelper.Rank(suburbViewModels, -33.874163, 151.209323).ForEach(o =>
            {
                o.Resturants = o.Resturants.OrderBy(f => f.distance).ThenByDescending(x => x.Ranking)
                    .ToList();
            });

            return View(suburbViewModels);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Resturant Ranking.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Contact Us.";

            return View();
        }

        [HttpPost]
        public ActionResult Create(ResturantFormViewModel newResturant)
        {
            if (newResturant != null && newResturant.File != null)
            {
                _resturantService.CreateResturant(new Resturant
                {
                    ResturantName = newResturant.ResturantName,
                    AveragePrice = newResturant.AveragePrice,
                    SuburbId = newResturant.ResturantSuburb,
                    StarRating = newResturant.StarRating,
                });

                string resturantPicture = System.IO.Path.GetFileName(newResturant.File.FileName);
                string path = System.IO.Path.Combine(Server.MapPath("~/images/"), resturantPicture);
                newResturant.File.SaveAs(path);

                _resturantService.SaveResturant();
            }

            var suburb = _suburbService.GetSuburb(newResturant.ResturantSuburb);
            return RedirectToAction("Index", new {suburb = suburb.SuburbName});
        }

        public ActionResult Filter(string suburb, string resturantName)
        {
            var resturants = _resturantService.GetResturantsBySuburb(suburb);

            var viewModelsuburb = Mapper.Map<IEnumerable<Resturant>, IEnumerable<ResturantViewModel>>(resturants);

            return View(viewModelsuburb);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using ResturantRanking.Service.Interfaces;
using ResturantRanking.Web;
using ResturantRanking.Web.Controllers;
using ResturantRanking.Web.Mappings;

namespace ResturantRanking.Web.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        [TestMethod]
        public void Index()
        {
            // Arrange
            AutoMapperConfiguration.Configure();
            var _suburbService = new Mock<ISuburb>().Object; ;
           var _resturantService = new Mock<IResturant>().Object;
           HomeController controller = new HomeController(_suburbService, _resturantService);

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void About()
        {
            // Arrange
            AutoMapperConfiguration.Configure();
            var _suburbService = new Mock<ISuburb>().Object; ;
            var _resturantService = new Mock<IResturant>().Object;
            HomeController controller = new HomeController(_suburbService, _resturantService);

            // Act
            ViewResult result = controller.About() as ViewResult;

            // Assert
            Assert.AreEqual("Resturant Ranking.", result.ViewBag.Message);
        }

        [TestMethod]
        public void Contact()
        {
            // Arrange
            AutoMapperConfiguration.Configure();
            var _suburbService = new Mock<ISuburb>().Object; ;
            var _resturantService = new Mock<IResturant>().Object;
            HomeController controller = new HomeController(_suburbService, _resturantService);

            // Act
            ViewResult result = controller.Contact() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void indexBySuburb()
        {
            // Arrange
            AutoMapperConfiguration.Configure();
            var _suburbService = new Mock<ISuburb>().Object; ;
            var _resturantService = new Mock<IResturant>().Object;
            HomeController controller = new HomeController(_suburbService, _resturantService);

            // Act
            ViewResult result = controller.Index("camperdown") as ViewResult;

            // Assert
            Assert.IsNotNull(result);

        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ResturantRanking.Data.DbFactory;
using ResturantRanking.Data.Interfaces;
using ResturantRanking.Model.Models;
using ResturantRanking.Service.Interfaces;

namespace ResturantRanking.Service.Services
{
    public class ResturantService : IResturant
    {
        private readonly IResturantRepository _resturantRepository;
        private readonly IOrderRepository _orderRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ISuburbRepository _suburbRepository;

        public ResturantService(IResturantRepository resturantRepository, 
            IOrderRepository orderRepository, ISuburbRepository suburbRepository, IUnitOfWork unitOfWork)
        {
            _resturantRepository = resturantRepository;
            _orderRepository = orderRepository;
            _suburbRepository = suburbRepository;
            _unitOfWork = unitOfWork;

        }
        #region IResturant members
        public IEnumerable<Resturant> GetResturants(string name = null)
        {
            return _resturantRepository.GetAll();
        }

        public IEnumerable<Resturant> GetResturantsBySuburb(string suburbName)
        {
            var resturants = _suburbRepository.GetSuburbByName(suburbName).Resturants.Where(g => g.ResturantName.ToLower().Contains(suburbName.ToLower().Trim()));
            return resturants;
        }

        public Resturant GetResturant(string name)
        {
            throw new NotImplementedException();
        }

        public void CreateResturant(Resturant resturant)
        {
            _resturantRepository.Add(resturant);
        }

        public void SaveResturant()
        {
            _unitOfWork.Commit();
        }
        #endregion
    }
}

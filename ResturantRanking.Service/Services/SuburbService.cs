﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ResturantRanking.Data.Interfaces;
using ResturantRanking.Model.Models;
using ResturantRanking.Service.Interfaces;

namespace ResturantRanking.Service.Services
{
    public class SuburbService : ISuburb
    {
        private readonly ISuburbRepository _suburbRepository;

        public SuburbService(ISuburbRepository suburbRepository)
        {
            _suburbRepository = suburbRepository;
        }
        #region ISuburb members

        public IEnumerable<Suburb> GetSuburbs(string postCode = null)
        {
            return string.IsNullOrEmpty(postCode) ? _suburbRepository.GetAll() : _suburbRepository.GetAll().Where(c => c.PostCode.ToString() == postCode);
        }

        public Suburb GetSuburb(int id)
        {
            return _suburbRepository.GetAll().FirstOrDefault(x => x.SuburbId == id);
        }

        public Suburb GetSuburb(string postCode)
        {
            throw new NotImplementedException();
        }

        public void CreateSuburb(Suburb suburb)
        {
            throw new NotImplementedException();
        }

        public void SaveSuburb()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}

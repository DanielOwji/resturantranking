﻿using System.Collections.Generic;
using ResturantRanking.Model.Models;

namespace ResturantRanking.Service.Interfaces
{
    public interface IOrderDetails
    {
        IEnumerable<OrderDetail> GetOrders();
        IEnumerable<OrderDetail> GetResturantOrders(string resturantName);
        OrderDetail GetOrders(int id);
        void CreateOrder(OrderDetail order);
        void SaveOrder();
    }
}
﻿using System.Collections.Generic;
using ResturantRanking.Model.Models;

namespace ResturantRanking.Service.Interfaces
{
    public interface ISuburb
    {
        IEnumerable<Suburb> GetSuburbs(string postCode = null);
        Suburb GetSuburb(int id);
        Suburb GetSuburb(string postCode);
        void CreateSuburb(Suburb suburb);
        void SaveSuburb();
    }
}
﻿using ResturantRanking.Model.Models;
using System.Collections.Generic;

namespace ResturantRanking.Service.Interfaces
{
    public interface IResturant
    {
        IEnumerable<Resturant> GetResturants(string name = null);
        Resturant GetResturant(string name);
        void CreateResturant(Resturant resturant);
        IEnumerable<Resturant> GetResturantsBySuburb(string SuburbName);
        void SaveResturant();

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ResturantRanking.Model.Models
{
    public class Resturant
    {
        public int ResturantId { get; set; }
        public string ResturantName { get; set; }
        public string Image { get; set; }
        public int StarRating { get; set; }
        public string Latidtude { get; set; }
        public string Longitude { get; set; }
        public virtual List<OrderDetail> OrdersDetails { get; set; }
        public double Ranking { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
        public int SuburbId { get; set; }
        public Suburb Suburb { get; set; }
        public Resturant()
        {
            DateCreated = DateTime.Now;
        }

        public decimal AveragePrice { get; set; }
        
    }
}

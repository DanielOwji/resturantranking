﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ResturantRanking.Model.Models
{
    public class OrderDetail
    {
        public int OrderDetailID { get; set; }

        public decimal Price { get; set; }

        public DateTime OrderTime { get; set; }

        public int ResturantId { get; set; }

        public virtual Resturant Suburb { get; set; }

        public DateTime? DateCreated { get; set; }

        public DateTime? DateUpdated { get; set; }

        public OrderDetail()
        {
            DateCreated = DateTime.Now;
        }
    }
}
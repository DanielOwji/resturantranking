﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ResturantRanking.Model.Models
{
    public class Suburb
    {
        public int SuburbId { get; set; }
        public int PostCode { get; set; }
        public string SuburbName { get; set; }
        public virtual List<Resturant> Resturants { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
        public Suburb()
        {
            DateCreated = DateTime.Now;
        }
    }
}

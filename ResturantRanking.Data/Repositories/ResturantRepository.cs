﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ResturantRanking.Data.DbFactory;
using ResturantRanking.Data.Interfaces;
using ResturantRanking.Model.Models;

namespace ResturantRanking.Data.Repositories
{
    public class ResturantRepository : RepositoryBase<Resturant>, IResturantRepository
    {
        public ResturantRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
        public Resturant GetResturantByName(string resturantName)
        {
            var resturant = this.DbContext.Resturants.FirstOrDefault(c => c.ResturantName == resturantName);
        
            return resturant;
        }
    }
}

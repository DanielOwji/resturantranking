﻿using ResturantRanking.Data.Interfaces;
using ResturantRanking.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ResturantRanking.Data.DbFactory;

namespace ResturantRanking.Data.Repositories
{
    public class SuburbRepository : RepositoryBase<Suburb>, ISuburbRepository
    {
        public SuburbRepository(IDbFactory dbFactory) : base(dbFactory)
        {
            
        }
        public override void Update(Suburb entity)
        {
            entity.DateUpdated = DateTime.Now;
            base.Update(entity);
        }
        public Suburb GetSuburbByName(string suburbName)
        {
            return this.DbContext.Suburbs.FirstOrDefault(c => c.SuburbName == suburbName);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ResturantRanking.Data.DbFactory;
using ResturantRanking.Data.Interfaces;
using ResturantRanking.Model.Models;

namespace ResturantRanking.Data.Repositories
{
    public class OrderRepository : RepositoryBase<OrderDetail>, IOrderRepository
    {
        public OrderRepository(IDbFactory dbFactory) : base(dbFactory)
        {
     
        }
        public OrderDetail GetOrderByResturantId(int id) => this.DbContext.OrderDetails.FirstOrDefault(c => c.ResturantId == id);

        public override void Update(OrderDetail entity)
        {
            entity.DateUpdated = DateTime.Now;
            base.Update(entity);
        }
    }
}

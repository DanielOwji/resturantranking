﻿using ResturantRanking.Data.DbFactory;
using ResturantRanking.Model.Models;

namespace ResturantRanking.Data.Interfaces
{
    public interface IResturantRepository : IRepository<Resturant>
    {
        Resturant GetResturantByName(string resturantName);
    }
}
﻿using ResturantRanking.Data.DbFactory;
using ResturantRanking.Model.Models;

namespace ResturantRanking.Data.Interfaces
{
    public interface ISuburbRepository : IRepository<Suburb>
    {
        Suburb GetSuburbByName(string suburbName);
    }
}
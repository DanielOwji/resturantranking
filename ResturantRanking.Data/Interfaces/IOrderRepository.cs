﻿using ResturantRanking.Data.DbFactory;
using ResturantRanking.Model.Models;

namespace ResturantRanking.Data.Interfaces
{
    public interface IOrderRepository : IRepository<OrderDetail>
    {
        OrderDetail GetOrderByResturantId(int id);
    }
}
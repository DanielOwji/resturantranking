﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ResturantRanking.Data.Configurations;
using ResturantRanking.Model.Models;

namespace ResturantRanking.Data
{
    public class ResturantRankingEntities : DbContext
    {
        public ResturantRankingEntities() : base("ResturantRankingEntities") { }                   
        public  DbSet<OrderDetail> OrderDetails { get; set; }
        public  DbSet<Resturant> Resturants { get; set; }
        public  DbSet<Suburb> Suburbs { get; set; }
        public virtual void Commit()
        {
            base.SaveChanges();
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new SuburbConfiguration());
            modelBuilder.Configurations.Add(new ResturantConfiguration());
            modelBuilder.Configurations.Add(new OrderConfiguration());
         
            
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ResturantRanking.Model.Models;

namespace ResturantRanking.Data
{
    public class ResturantRankingData : DropCreateDatabaseIfModelChanges<ResturantRankingEntities>
    {
        protected override void Seed(ResturantRankingEntities context)
        {
            GetSuburbs().ForEach(s => context.Suburbs.Add(s));
            context.Commit();
            GetResturants().ForEach(r => context.Resturants.Add(r));
            context.Commit();
            GetOrders().ForEach(o => context.OrderDetails.Add(o));

            context.Commit();
        }

        public static List<Suburb> GetSuburbs()
        {
            return new List<Suburb>
            {
                new Suburb
                {
                    SuburbName = "Camperdown",
                    PostCode = 2050
                },
                new Suburb
                {
                    SuburbName = "North Sydney",
                    PostCode = 2060
                },
                new Suburb
                {
                    SuburbName = "Crows Nest",
                    PostCode = 2065
                }
            };
        }

        public static List<Resturant> GetResturants()
        {
            return new List<Resturant>
            {
                new Resturant
                {
                    ResturantName = "Ciao Italia",
                    SuburbId = 1,
                    StarRating = 3,
                    Image = "ciao-italia.jpg",
                    Latidtude = "-33.886057",
                    Longitude = "151.179086",
                    AveragePrice = 40
                    
                },
                new Resturant
                {
                    ResturantName = "Murano",
                    SuburbId = 1,
                    StarRating = 4,
                    Image = "murano.jpg",
                    Latidtude = "-33.882482",
                    Longitude = "151.181445",
                    AveragePrice = 80

                },
                new Resturant
                {
                    ResturantName = "Bouet",
                    SuburbId = 1,
                    StarRating = 3,
                    Image = "bouet.jpg",
                    Latidtude = "-33.888482",
                    Longitude = "151.180115",
                    AveragePrice = 50

                },
                new Resturant
                {
                    ResturantName = "Royals",
                    SuburbId = 1,
                    StarRating = 3,
                    Image = "ciao-italia.jpg",
                    Latidtude = "-33.886057",
                    Longitude = "151.179786",
                    AveragePrice = 50

                },
                new Resturant
                {
                    ResturantName = "Dim Sim",
                    SuburbId = 2,
                    StarRating = 5,
                    Image = "dim-sim.jpg",
                    Latidtude = "-33.839040",
                    Longitude = "151.206538",
                    AveragePrice = 35
                },
                new Resturant
                {
                    ResturantName = "Amelie",
                    SuburbId = 3,
                    StarRating = 4,
                    Image = "amelie.jpg",
                    Latidtude = "-33.825998",
                    Longitude = "151.200961",
                    AveragePrice = 100
                }
            };
        }

        public static List<OrderDetail> GetOrders()
        {
            return new List<OrderDetail>
            {
                new OrderDetail
                {
                    ResturantId = 1,
                    Price = 46.99m,
                    OrderTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month - 1, DateTime.Now.Day - 6)
                },
                new OrderDetail
                {
                    ResturantId = 1,
                    Price = 120.95m,
                    OrderTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month - 1, DateTime.Now.Day - 3)
                },
                new OrderDetail
                {
                    ResturantId = 1,
                    Price = 59.99m,

                    OrderTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month - 1, DateTime.Now.Day)
                },
                new OrderDetail
                {
                    ResturantId = 2,
                    Price = 54.99m,
                    OrderTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month - 3, DateTime.Now.Day)
                },
                new OrderDetail
                {
                    ResturantId = 2,
                    Price = 47.99m,
                    OrderTime = new DateTime(DateTime.Now.Year-1, 7, 10)
                },
                new OrderDetail
                {
                    ResturantId = 2,
                    Price = 94.99m,
                    OrderTime = new DateTime(DateTime.Now.Year, 3, 21)
                },
                new OrderDetail
                {
                    ResturantId = 3,
                    Price = 249.5m,
                    OrderTime = new DateTime(DateTime.Now.Year-1, 6, 5)
                },
                new OrderDetail
                {
                    ResturantId = 3,
                    Price = 299.95m,
                    OrderTime = new DateTime(DateTime.Now.Year-1, 6, 20)
                },
                new OrderDetail
                {
                    ResturantId = 4,
                    Price = 308.00m,
                    OrderTime = new DateTime(DateTime.Now.Year-1, 5, 1)
                },
                new OrderDetail
                {
                    ResturantId = 4,
                    Price = 299.95m,
                    OrderTime = new DateTime(DateTime.Now.Year-1, 4, 2)
                },
                new OrderDetail
                {
                    ResturantId = 5,
                    Price = 202.99m,
                    OrderTime = new DateTime(DateTime.Now.Year-1, 3, 5)
                },
                new OrderDetail
                {
                    ResturantId = 6,
                    Price = 63.99m,
                    OrderTime = new DateTime(DateTime.Now.Year, 2, 8)
                },
                new OrderDetail
                {
                    ResturantId = 6,
                    Price = 177.99m,
                    OrderTime = new DateTime(DateTime.Now.Year, 2, 11)
                },
                new OrderDetail
                {
                    ResturantId = 2,
                    Price = 54.99m,
                    OrderTime = new DateTime(DateTime.Now.Year, 8, 9)
                },
                new OrderDetail
                {
                    ResturantId = 5,
                    Price = 133.99m,
                    OrderTime = new DateTime(DateTime.Now.Year, 6, 17)
                }
            };
        }
    }
}
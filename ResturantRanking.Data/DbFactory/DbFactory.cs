﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResturantRanking.Data.DbFactory
{
    public class DbFactory : Disposable, IDbFactory
    {
        ResturantRankingEntities _dbContext;

        public ResturantRankingEntities Init()
        {
            return _dbContext ?? (_dbContext = new ResturantRankingEntities());
        }

        protected override void DisposeCore()
        {
            _dbContext?.Dispose();
        }
    }
}

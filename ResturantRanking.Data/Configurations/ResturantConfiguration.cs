﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ResturantRanking.Model.Models;

namespace ResturantRanking.Data.Configurations
{
    public class ResturantConfiguration : EntityTypeConfiguration<Resturant>
    {
        public ResturantConfiguration()
        {
            ToTable("Resturants");
            Property(c => c.ResturantName).IsRequired().HasMaxLength(50);
            Property(g => g.AveragePrice).IsRequired().HasPrecision(8, 2);
            Property(d => d.SuburbId).IsRequired();
        }
    }
}

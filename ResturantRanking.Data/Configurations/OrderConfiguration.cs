﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ResturantRanking.Model.Models;

namespace ResturantRanking.Data.Configurations
{
    public class OrderConfiguration : EntityTypeConfiguration<OrderDetail>
    {
        public OrderConfiguration()
        {
            ToTable("OrderDetails");
            Property(c => c.Price).IsRequired();
            Property(c => c.ResturantId).IsRequired();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ResturantRanking.Model.Models;

namespace ResturantRanking.Data.Configurations
{
    public class SuburbConfiguration : EntityTypeConfiguration<Suburb>
    {
        public SuburbConfiguration()
        {
            ToTable("Suburbs");
            Property(c => c.SuburbName).IsRequired().HasMaxLength(50); ;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NUnit.Framework;
using ResturantRanking.Data.DbFactory;
using ResturantRanking.Data.Interfaces;
using ResturantRanking.Model.Models;
using ResturantRanking.Service.Interfaces;
using ResturantRanking.Service.Services;
using Assert = NUnit.Framework.Assert;
using Microsoft.CSharp;
using ResturantRanking.Data;

namespace ResturantRanking.Service.Test
{
    [TestFixture]
    public class ServiceTest
    {
        #region Variables
        IResturant _resturantService;
        ISuburb _suburbService;
        IResturantRepository _resturantRepository;
        ISuburbRepository _suburbRepository;
        IOrderRepository _orderRepository;
        IUnitOfWork _unitOfWork;
        List<Resturant> _randomResturants;
        List<Suburb> _randomSuburbs;
        #endregion

        #region setup
        [SetUp]
        public void Setup()
        {

            _randomResturants = SetupResturants();
            _resturantRepository = SetupResturantRepository();
            _unitOfWork = new Mock<IUnitOfWork>().Object;
            _resturantService = new ResturantService(_resturantRepository, _orderRepository, _suburbRepository, _unitOfWork);

            _suburbRepository = SetupSuburbRepository();
            _unitOfWork = new Mock<IUnitOfWork>().Object;
            _suburbService = new SuburbService( _suburbRepository);

        }
        public List<Resturant> SetupResturants()
        {
            var counter = new int();
            var resturants = ResturantRankingData.GetResturants();

            foreach (var Resturant in resturants)
                Resturant.ResturantId = ++counter;

            return resturants;
        }

        public IResturantRepository SetupResturantRepository()
        {
            // Init repository
            var repo = new Mock<IResturantRepository>();

            // Setup mocking behavior
            repo.Setup(r => r.GetAll()).Returns(_randomResturants);

            repo.Setup(r => r.GetById(It.IsAny<int>()))
                .Returns(new Func<int, Resturant>(
                    id => _randomResturants.Find(a => a.ResturantId.Equals(id))));

            repo.Setup(r => r.Add(It.IsAny<Resturant>()))
                .Callback(new Action<Resturant>(newResturant =>
                {
                    dynamic maxResturantId = _randomResturants.Last().ResturantId;
                    dynamic nextResturantID = maxResturantId + 1;
                    newResturant.ResturantId = nextResturantID;
                    newResturant.DateCreated = DateTime.Now;
                    _randomResturants.Add(newResturant);
                }));

            repo.Setup(r => r.Update(It.IsAny<Resturant>()))
                .Callback(new Action<Resturant>(x =>
                    {
                        var oldResturant = _randomResturants.Find(a => a.ResturantId == x.ResturantId);
                        oldResturant.DateCreated = DateTime.Now;
                        oldResturant = x;
                    }));

            repo.Setup(r => r.Delete(It.IsAny<Resturant>()))
                .Callback(new Action<Resturant>(x =>
                {
                    var _ResturantToRemove = _randomResturants.Find(a => a.ResturantId == x.ResturantId);

                    if (_ResturantToRemove != null)
                        _randomResturants.Remove(_ResturantToRemove);
                }));

            // Return mock implementation
            return repo.Object;
        }

        public ISuburbRepository SetupSuburbRepository()
        {
            // Init repository
            var repo = new Mock<ISuburbRepository>();

            // Setup mocking behavior
            repo.Setup(r => r.GetAll()).Returns(_randomSuburbs);

            repo.Setup(r => r.GetById(It.IsAny<int>()))
                .Returns(new Func<int, Suburb>(
                    id => _randomSuburbs.Find(a => a.SuburbId.Equals(id))));

            repo.Setup(r => r.Add(It.IsAny<Suburb>()))
                .Callback(new Action<Suburb>(newSuburb =>
                {
                    dynamic maxSuburbId = _randomSuburbs.Last().SuburbId;
                    dynamic nextSuburbId = maxSuburbId + 1;
                    newSuburb.SuburbId = nextSuburbId;
                    newSuburb.DateCreated = DateTime.Now;
                    _randomSuburbs.Add(newSuburb);
                }));

            repo.Setup(r => r.Update(It.IsAny<Suburb>()))
                .Callback(new Action<Suburb>(x =>
                {
                    var oldSuburb = _randomSuburbs.Find(a => a.SuburbId == x.SuburbId);
                    oldSuburb.DateCreated = DateTime.Now;
                    oldSuburb = x;
                }));

            repo.Setup(r => r.Delete(It.IsAny<Suburb>()))
                .Callback(new Action<Suburb>(x =>
                {
                    var _suburbToRemove = _randomSuburbs.Find(a => a.SuburbId == x.SuburbId);

                    if (_suburbToRemove != null)
                        _randomSuburbs.Remove(_suburbToRemove);
                }));

            // Return mock implementation
            return repo.Object;
        }

        #endregion

        //ServiceShouldReturnAllResturant Test        
        [Test]
        public void ServiceShouldReturnAllResturant()
        {
            var resturants = _resturantService.GetResturants();

            Assert.That(resturants, Is.EqualTo(_randomResturants));
        }

        [Test]
        public void ServiceShouldReturnSuburbs()
        {
            var suburbs = _suburbService.GetSuburbs();

            Assert.That(suburbs, Is.EqualTo(_randomSuburbs));
        }

    }
}
